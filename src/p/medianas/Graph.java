package p.medianas;

public class Graph {
    private int numVertex;
    private int matrixAdj[][];
    private int dist[][];
    private int pred[][];
    private int infinity = 9999999;
    
    public Graph(int numVertex) {
        this.numVertex = numVertex;
        this.matrixAdj = new int[numVertex][numVertex];
        this.pred = new int[numVertex][numVertex];        
        this.dist = new int[numVertex][numVertex];
        
        this.init();
    }
    
    private void init() {
        for (int i=0; i < this.numVertex; i++) {
            for (int j=0; j < this.numVertex; j++) {
                if (i != j) {
            		this.matrixAdj[i][j] = this.infinity;
            	} else {
            		this.matrixAdj[i][j] = 0;
            	}
            }
        }
    }
    
    public int getNumVertex() {
        return(this.numVertex);
    }
    
    public void addEdge(int x, int y, int weight) {
        if ((weight >= 0) && ((x < this.numVertex) && (y < this.numVertex)) && ((x >= 0) && (y >= 0))) {
            this.matrixAdj[x][y] = weight;
            this.matrixAdj[y][x] = weight;
        } else {
            System.out.println("Error");
        }
    }
   
    public int getWeight(int x, int y) {
        if ((x < this.numVertex && y < this.numVertex) && ((x >= 0) && (y >= 0))) {
            return(this.matrixAdj[x][y]);
        } else {
            System.out.println("Error");
            return(-1);
        }
    }
    
    public void printGraph () {
        for (int i=0; i < this.numVertex; i++) {
            for (int j=0; j <= i; j++) {
                System.out.print(matrixAdj[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public void printDist () {
        for (int i=0; i < this.numVertex; i++) {
            //for (int j=0; j <= i; j++) {
            for (int j=0; j < this.numVertex; j++) {
                System.out.print(this.dist[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    private void copyDist() {        
        for (int i = 0; i < numVertex; i++) {
            for (int j = 0; j < numVertex; j++) {
                dist[i][j] = matrixAdj[i][j];
            }
        }
    }
    
    public void floyd() {
        copyDist();
        int i, j, k;            
        for (i=0; i < this.numVertex; i++) {         
            for (j=0; j < this.numVertex; j++) {
                if (dist[i][j] < infinity && i!=j) {
                    pred[i][j] = i;
                } else {
                    pred[i][j] = -1;
                }
            }
        }             
        for (k=0; k < this.numVertex; k++) {
            for (i=0; i < this.numVertex; i++) {
                for (j=0; j < this.numVertex; j++) {
                    if (i!=j && (dist[i][j] > dist[i][k] + dist[k][j])) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                        int aux = pred[k][j];
                        pred[i][j] = aux;
                    }
                }
            }
        }             
    }
}
