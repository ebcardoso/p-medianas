package p.medianas;

public class P_Medianas {
    private int n;
    private int p;    
    private int comb[];

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }    
    
    public P_Medianas (int n, int p) {
        this.n = n;
        this.p = p;        
        this.comb = new int[p];
        
        for (int i=0; i < this.p; i++) {
            this.comb[i] = 0;
        }
    }
    
    private void printar() {
        for (int i=0; i < this.p; i++) {
            System.out.print(" - " + this.comb[i]);
        }
        System.out.println();
    }
    
    private boolean iguais9() {
        int contador = 0;
        for (int i = 0; i < this.p; i++) {
            if (this.comb[i] == (this.n-1)) {
                contador++;
            }
        }
        if (contador == this.p) {
            return(false);
        }        
        return(true);
    }
    
    public void run() {
        perm(this.p - 1); //(p-1) para não estourar o índice do vetor
    }
    
    //gerar todas as permutações (n, p) possívei
    private void perm(int pos) {
        if (!iguais9()) {
            return;
        }
        
        printar();
        
        if (this.comb[pos] != (this.n-1)) {
            this.comb[pos]++;
        } else {
            int x = pos;            
            while (true) {
                this.comb[x] = 0;
                if (x > 0) {
                    if (this.comb[x-1] == (this.n-1)) {
                        x--;
                    } else {
                        this.comb[x-1]++;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        perm(this.p - 1); //(p-1) para não estourar o índice do vetor
    }
}